FROM php:8.2-apache-buster

COPY index.php /var/www/html/index.php
COPY kaamelott-arthur.gif /var/www/html/kaamelott-arthur.gif

EXPOSE 80

CMD ["apache2-foreground"]